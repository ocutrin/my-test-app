import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-test-app-app',
  templateUrl: 'my-test-app.component.html',
  styleUrls: ['my-test-app.component.css']
})
export class MyTestAppAppComponent {
  title = 'my-test-app works!';
}
