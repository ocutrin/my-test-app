import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { MyTestAppAppComponent } from '../app/my-test-app.component';

beforeEachProviders(() => [MyTestAppAppComponent]);

describe('App: MyTestApp', () => {
  it('should create the app',
      inject([MyTestAppAppComponent], (app: MyTestAppAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'my-test-app works!\'',
      inject([MyTestAppAppComponent], (app: MyTestAppAppComponent) => {
    expect(app.title).toEqual('my-test-app works!');
  }));
});
