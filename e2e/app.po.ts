export class MyTestAppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('my-test-app-app h1')).getText();
  }
}
